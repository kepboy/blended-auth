﻿using Microsoft.AspNetCore.Authentication;

namespace MainApp
{
    internal static class AuthContextExtensions
    {
        public static Task LogEvent<TOptions>(this BaseContext<TOptions> context, string eventName) where TOptions : AuthenticationSchemeOptions
        {
            var logger = context.HttpContext
                                .RequestServices
                                .GetRequiredService<ILoggerFactory>()
                                .CreateLogger(typeof(Program));

            logger.LogInformation(eventName);

            return Task.CompletedTask;
        }
    }
}
