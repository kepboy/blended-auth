﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MainApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ValueController : ControllerBase
    {
        public async Task<ActionResult<Weather>> Get(CancellationToken cancel)
        {
            await Task.Delay(5000, cancel);

            return new Weather
            {
                Temp = r.Next(-6, 50),
                When = DateTime.Now.Date,
            };
        }

        private static Random r = new Random();
    }
}
