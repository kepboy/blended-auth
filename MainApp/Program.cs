using MainApp.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MainApp
{
    public class Program
    {
        private static JwtSecurityToken CreateJwtToken(string issuer, string aud, string signingKey, TimeSpan expiresIn, ClaimsPrincipal user)
        {
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            
            return new JwtSecurityToken(issuer: issuer,
                                        audience: aud,
                                        claims: user.Claims,
                                        expires: DateTime.UtcNow + expiresIn,
                                        signingCredentials: signingCredentials);
        }

        private static void IssueAccessToken(PrincipalContext<CookieAuthenticationOptions> context)
        {
            var configuration = context.HttpContext.RequestServices.GetRequiredService<IConfiguration>();
            var issuer = configuration["JWT:Issuer"];
            var audience = configuration["JWT:Audience"];
            var signingKey = configuration["JWT:Key"];
            var jwtSecurityToken = CreateJwtToken(issuer, audience, signingKey, context.Options.ExpireTimeSpan, context.Principal!);
            var accessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);

            context.Options.CookieManager.AppendResponseCookie(context.HttpContext, "km-test", accessToken, new CookieOptions
            {
                HttpOnly = false,
                Expires = DateTimeOffset.UtcNow + context.Options.ExpireTimeSpan,
            });
        }

        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ?? throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");
            builder.Services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));
            builder.Services.AddDatabaseDeveloperPageExceptionFilter();

            builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<ApplicationDbContext>();

            builder.Services.AddAuthentication()
                            .AddJwtBearer(opt =>
                            {
                                var issuer = builder.Configuration["JWT:Issuer"];
                                var audience = builder.Configuration["JWT:Audience"];
                                var signingKey = builder.Configuration["JWT:Key"];

                                opt.RequireHttpsMetadata = false;
                                opt.SaveToken = false;
                                opt.TokenValidationParameters = new TokenValidationParameters
                                {
                                    ValidateIssuerSigningKey = true,
                                    ValidateIssuer = true,
                                    ValidateAudience = true,
                                    ValidateLifetime = true,
                                    ClockSkew = TimeSpan.Zero,
                                    ValidIssuer = issuer,
                                    ValidAudience = audience,
                                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(signingKey))
                                };
                            });
            builder.Services.ConfigureApplicationCookie(opt =>
            {
                // TODO: Handle sign out
                opt.Cookie.Name = "kepsquare";
                opt.ExpireTimeSpan = TimeSpan.FromSeconds(120);
                opt.Events.OnSigningIn = ctx =>
                {
                    var result = ctx.LogEvent("OnSigningIn - issue JWT");

                    IssueAccessToken(ctx);

                    return result;
                };
                opt.Events.OnCheckSlidingExpiration = ctx =>
                {
                    var result = ctx.LogEvent("OnCheckSlidingExpiration");

                    if(ctx.ShouldRenew)
                    {
                        ctx.LogEvent("Re-issuing JWT");
                        IssueAccessToken(ctx);
                    }

                    return result;
                };
            });


            builder.Services.AddRazorPages();
            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if(app.Environment.IsDevelopment())
            {
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.MapRazorPages();
            app.MapControllers();

            app.Run();
        }
    }
}