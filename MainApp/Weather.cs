﻿namespace MainApp
{
    public class Weather
    {
        public int Temp { get; set; }
        public DateTime When { get; set; }
    }
}
