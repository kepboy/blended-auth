﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function getWeather(id, token) {
    const element = document.getElementById(id);
    let headers = {};

    if(token)
        headers.Authorization = `bearer ${token}`;

    if(element) {
        fetch('api/value', { headers: headers }).then(res => res.json())
                                                .then(weather => element.innerHTML = `temp on ${weather.when}: ${weather.temp}`)
                                                .catch(ex => element.innerHTML = JSON.stringify(ex));
    } else {
        console.log(`element ${id} not found`);
    }
}
